Feature: Create Account

@createAccount @smoke
  Scenario: I am creating a new account with existing email id
    Given Rediff application
     When I click on 'Create Account' link
      And I enter first name
      And I enter rediff id
     | FullName | Rediff Id |
     | manidhar | manidhar  |
      And I click on Check Availability button
     Then I validate the error message should be as 'Sorry, the ID that you are looking for is taken.' at "//div[@id='check_availability']/font[1]/b888" with "xpath"
 