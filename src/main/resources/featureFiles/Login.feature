Feature: Login Feature Testing

@smoke @login
  Scenario: I login to application with invalid credentials
    Given Rediff application
     When I click on 'Sign in' link
      And I enter invalid user name
      And I enter invalid password
      And I click on Sign in button
     Then I validate the error message should be as 'Wrong username and password combination.' at "div_login_error" with "id"
  
  @regression @login @smoke
  Scenario Outline: I login to application with invalid credentials with multipe users
    Given Rediff application
     When I click on 'Sign in' link
      And I enter invalid user name as "<username>"
      And I enter invalid password as "<password>"
      And I click on Sign in button
     Then I validate the error message should be as 'Wrong username and password combination.787878' at "div_login_error" with "id"
      Examples: 
      | username         | password |
      | sdfdsf@sdfdf.sdo | pandjfsd |
      | sdjjsf@sdfdf.sdo | pwwdjfsd |
      | sdllsf@sdfdf.sdo | pqqdjfsd |
      | sdppsf@sdfdf.sdo | plldjfsd |
      