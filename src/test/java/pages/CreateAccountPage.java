package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateAccountPage {
	WebDriver driver;

	@FindBy(xpath = "//td[contains(text(),'Full Name')]/parent::tr/td[3]/input")
	WebElement fullNameInputField;

	@FindBy(xpath = "//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input[1]")
	WebElement rediffIdInputField;

	@FindBy(xpath = "//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input[2]")
	WebElement checkAvailibilityButton;

	public CreateAccountPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void enterFullName(String fullName) {
		fullNameInputField.sendKeys(fullName);
	}

	public void enterRediffId(String rediffId) {
		rediffIdInputField.sendKeys(rediffId);
	}

	public void clickOnCheckAvailibility() {
		checkAvailibilityButton.click();
	}
}
