package stepdefinitions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {
	protected static WebDriver driver;
	Properties env;
	private static Logger logger=LogManager.getLogger(Hooks.class.getClass());
	String path=System.getProperty("user.dir");

	@Before
	public void openBrowser() throws IOException {
		InputStream fis=new FileInputStream( path+"\\src\\main\\resources\\EnvironmentDetails.properties");
		env=new Properties();
		env.load(fis);
		logger.info("Property files loaded");
		
		String browserName=null;
		String url=null;
		String debugMode=env.getProperty("debugMode");
		
		if(debugMode.equals("on")) {
			browserName=env.getProperty("browser");
			url=env.getProperty("url");
		}else {
			browserName=System.getProperty("browser");
			url=System.getProperty("url");
		}
		
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					path+"\\src\\main\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			logger.debug("Opened the Chrome Browser");
		}else if(browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", path+"\\src\\main\\resources\\drivers\\geckodriver.exe");
			driver=new FirefoxDriver();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			logger.debug("Opened the Firefox Browser");

		}else if(browserName.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver", path+"\\src\\main\\resources\\drivers\\msedgedriver.exe");
			driver=new EdgeDriver();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			logger.debug("Opened the Edge Browser");

		}else if(browserName.equalsIgnoreCase("opera")) {
			System.setProperty("webdriver.opera.driver", path+"\\src\\main\\resources\\drivers\\operadriver.exe");
			driver=new OperaDriver();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			logger.debug("Opened the Opera Browser");

		}else {
			throw new Error("Invalid Browser name provided, cross check the name");
		}
	}

	@After
	public void closeBrowser() {
		driver.close();
		env.clear();
		logger.debug("Closing the Browser");

	}
}
