package stepdefinitions;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import pages.CreateAccountPage;

public class CreateAccountSteps {
	WebDriver driver = Hooks.driver;
	private static Logger logger=LogManager.getLogger(CreateAccountSteps.class.getClass());
	CreateAccountPage createAccountPage=new CreateAccountPage(driver);
	@When("I enter first name")
	public void i_enter_first_name() {
		
		createAccountPage.enterFullName("manidhar");
		logger.debug("I have entered first name");
	}

	@When("I enter rediff id")
	public void i_enter_rediff_id(DataTable dt) {
		List<List<String>> data=dt.asLists();
		createAccountPage.enterRediffId(data.get(1).get(0));
		logger.debug("I have entered email id ");

	}

	@When("I click on Check Availability button")
	public void i_click_on_check_availability_button() throws InterruptedException {
		try {
		Thread.sleep(1500);
		JavascriptExecutor js=(JavascriptExecutor)driver; 
		createAccountPage.clickOnCheckAvailibility();
		logger.debug("I have clicked on Check availibility button");
		}catch(Exception e) {
			logger.debug(e);
		}
	}

}
