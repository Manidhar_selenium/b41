package stepdefinitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.*;
import junit.framework.Assert;
import pages.LoginPage;

public class RediffLoginSteps {
	WebDriver driver = Hooks.driver;
	private static Logger logger = LogManager.getLogger(RediffLoginSteps.class.getClass());
	LoginPage loginPage = new LoginPage(driver);

	@Given("Rediff application")
	public void rediff_application() {
		String actualPageTitle = driver.getTitle();
		String expectedPageTitle = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
		if (actualPageTitle.equals(expectedPageTitle)) {
			System.out.println("Page title matched");
		} else {
			System.out.println("Page titles not matched");
		}
	}

	@When("I click on {string} link")
	public void i_click_on_link(String linkText) {
		driver.findElement(By.linkText(linkText)).click();
	}

	@When("I enter invalid user name")
	public void i_enter_invalid_user_name() {
		loginPage.enterUserName("sdfdsf@sdfdf.sdo");
	}

	@When("I enter invalid password")
	public void i_enter_invalid_password() {
		loginPage.enterPassword("sdfdsfkso");
	}

	@When("I click on Sign in button")
	public void i_click_on_sign_in_button() {
		loginPage.clickOnSignInButton();
	}

	@When("I enter invalid user name as {string}")
	public void i_enter_invalid_user_name_as(String emailId) {
		loginPage.enterUserName(emailId);
	}

	@When("I enter invalid password as {string}")
	public void i_enter_invalid_password_as(String password) {
		loginPage.enterPassword(password);
	}

	@Then("I validate the error message should be as {string} at {string} with {string}")
	public void i_validate_the_error_message_should_be_as_at_with(String expectedErrorMessage, String locatorValue,
			String locatorName) throws InterruptedException {
		try {
			Thread.sleep(1500);
			String actualErrorMessage = null;
			if (locatorName.equals("id")) {
				actualErrorMessage = driver.findElement(By.id(locatorValue)).getText();
			} else if (locatorName.equals("xpath")) {
				actualErrorMessage = driver.findElement(By.xpath(locatorValue)).getText();
			}

			Assert.assertEquals(expectedErrorMessage, actualErrorMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

}
