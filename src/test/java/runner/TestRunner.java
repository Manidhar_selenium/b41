package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/cucumberReports.html",
				  "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
				},
		features = {"src/main/resources/featureFiles"},
		glue={"stepdefinitions"},
		monochrome = true,
		//tags = "@createAccount",
		dryRun = false,
		publish = true
		)
//Comment1
public class TestRunner {

}
